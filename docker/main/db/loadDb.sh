#!/bin/sh
set -e

echo "Loading open_lmis database..."
# dropdb will likely fail, since there is no db set up yet. Ignore those errors
dropdb -U postgres open_lmis || \
createdb -U postgres open_lmis
psql -U postgres open_lmis -f open_lmis.dump
echo "... db loaded"
