####################################
#   Stage 1: Builder
####################################
FROM casper/elmis_builder AS builder

####################################
#   Stage 2: Running the app
####################################
FROM casper/elmis_base
LABEL maintainer="Chongsun Ahn <chongsun.ahn@villagereach.org>"

ARG KEEP_OR_WIPE=wipe
ARG SAVED_DB_DUMP=db/saved_db.dump
RUN echo "KEEP_OR_WIPE=$KEEP_OR_WIPE"
RUN echo "SAVED_DB_DUMP=$SAVED_DB_DUMP"

###############################################
#   Section copied from Debezium image
###############################################
# install Debezium connector dependencies
# This is based on the Debezium-maintained docker image here:
# https://github.com/debezium/docker-images/blob/master/postgres/9.6/Dockerfile
ARG USE_POSTGIS=true
ENV PLUGIN_VERSION=v0.10.0.Beta2

ENV WAL2JSON_COMMIT_ID=92b33c7d7c2fccbeb9f79455dafbc92e87e00ddd

# Install the packages which will be required to get everything to compile
RUN apt-get update \
    && apt-get install -f -y --no-install-recommends \
        software-properties-common \
        build-essential \
        pkg-config \
        git \
# The docker image I took this from was on Debian stretch, which is oldstable.
# In buster, libpq-dev has gotten a major update that causes issues
        libpq-dev/stretch-pgdg \
        postgresql-server-dev-9.6 \
        libproj-dev \
    && if [ "$USE_POSTGIS" != "false" ]; then apt-get install -f -y --no-install-recommends \
        postgresql-9.6-postgis-2.4 \
        postgresql-9.6-postgis-2.4-scripts \
        postgis; \
       fi \
    && apt-get install -f -y --no-install-recommends \
        liblwgeom-dev \
        libprotobuf-c-dev \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Compile the plugin from sources and install it
RUN git clone https://github.com/debezium/postgres-decoderbufs -b $PLUGIN_VERSION --single-branch \
    && cd /postgres-decoderbufs \
    && make && make install \
    && cd / \
    && rm -rf postgres-decoderbufs

RUN git clone https://github.com/eulerto/wal2json -b master --single-branch \
    && cd /wal2json \
    && git checkout $WAL2JSON_COMMIT_ID \
    && make && make install \
    && cd / \
    && rm -rf wal2json
# Copy the custom configuration which will be passed down to the server (using a .sample file is the preferred way of doing it by
# the base Docker image)
COPY postgresql.conf.sample $PGDATA/postgresql.conf
###############################################
#   End section copied from Debezium image
###############################################

# deploy db
USER root
ADD db /open-lmis-db
WORKDIR /open-lmis-db
COPY --from=builder build-outputs/pg.dump fresh_db.dump
COPY $SAVED_DB_DUMP saved_db.dump
RUN if [ "$KEEP_OR_WIPE" = "wipe" ]; then \
        mv fresh_db.dump open_lmis.dump; \
    elif [ "$KEEP_OR_WIPE" = "keep" ]; then \
        mv saved_db.dump open_lmis.dump; \
    else \
        echo "KEEP_OR_WIPE must be \"keep\" or \"wipe\"" \
        && exit 1; \
    fi
RUN su postgres -c "pg_ctl restart -D $PGDATA -swt 300" && \
    /bin/sh loadDb.sh

# get tomcat
ADD tomcat/tomcat.sh /etc/init.d/tomcat
RUN chmod u+x /etc/init.d/tomcat && \
    useradd openlmis --home-dir=/home/openlmis && \
    mkdir -p /home/openlmis && \
    chown -R openlmis:openlmis /home/openlmis && \
    cd /home/openlmis && \
    wget http://archive.apache.org/dist/tomcat/tomcat-7/v7.0.55/bin/apache-tomcat-7.0.55.tar.gz && \
    tar -xvzf apache-tomcat-7.0.55.tar.gz && \
    rm apache-tomcat-7.0.55.tar.gz && \
    ln -s apache-tomcat-7.0.55 apache-tomcat && \
    chown -R openlmis:openlmis apache-tomcat-7.0.55
ENV TOMCAT_HOME /home/openlmis/apache-tomcat/
ADD tomcat/server.xml /home/openlmis/apache-tomcat/conf/
ADD tomcat/tomcat-users.xml.template $TOMCAT_HOME/conf/
ADD tomcat/configureTomcat.sh $TOMCAT_HOME/
RUN chmod u+x $TOMCAT_HOME/configureTomcat.sh

# get OpenLMIS
COPY --from=builder build-outputs/openlmis-web.war /home/openlmis/openlmis-web.war
RUN cd /home/openlmis && \
    chown openlmis:openlmis openlmis-web.war && \
    rm -Rf apache-tomcat/webapps/ROOT* && \
    cp openlmis-web.war apache-tomcat/webapps/ROOT.war

# Ports for tomcat (8080) and postgresql (5432)
EXPOSE 8080 5432

# set command to run on container start
USER root
ADD start.sh /sbin/start.sh
RUN chmod u+x /sbin/start.sh
CMD ["/sbin/start.sh"]
