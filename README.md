casper-elmis
================

Docker images for eLMIS/v2 for Project Casper.

# About

**These containers are not intended for production deployments.**

The design of this repository uses:

- A base image built from `docker/base/`
- eLMIS source code placed in `src`


# Directory Structure

### `docker/`

Contains setup and run scripts, configuration files and Dockerfile for the main app image, `casper/elmis`.

### `docker/base/`

Dockerfile for base image, `casper/elmis_base` **build this first**

### `docker/builder/`

This docker image, `casper/elmis_builder` is used to build the app; it also depends on the base image.

### `docker/postgres/`

Configuration files for included postgres.

### `docker/tomcat/`

Configuration files for included tomcat.

### `docker/builder/src`

Contains eLMIS source code that will be used by the builder image. **You must provide this**


# Requirements

- Docker
- Git


# Getting Started

1. Build the base image (Postgres 9.6 and Java 8 on top of CentOS 7), and compile eLMIS:

    ```shell
    docker-compose -f docker-compose.builder.yml build baseimage buildimage
    ```

2. Build the main image (configures the eLMIS runtime environment):

    ```shell
    docker-compose -f docker-compose.builder.yml build image
    ```

3. Run the eLMIS container.

    ```shell
    docker-compose up -d
    ```

To determine the url to access the local instance of OpenLMIS from your browser you need to know the IP address of the local instance and the port number mapped to http.

To determine the ip address, at a non-docker command prompt.

```shell
hostname [-I]
```

To determine the port mapping at a command prompt within docker type.

```shell
docker ps
```

From a browser on your local machine you would access OpenLMIS with a URL like.

```shell
http://192.168.59.103:80/
```

# Useful volumes:

`/var/logs`

contains system logs

`/home/openlmis`

contains tomcat running OpenLMIS which includes Tomcat logs as well as OpenLMIS logs.

# Useful ports

`5432` is postgres which will allow all connections with a password.

**it's not recommended to expose postgres to the public internet**

`8080` is Tomcat which runs http to OpenLMIS.
